---
title: ""
icon: "fa-group"
weight: 1
---

<picture>
<source src="logo.webp" class="center img-fluid" width="300" height="300" type="image/webp">
<source src="logo.jpeg" class="center img-fluid" width="300" height="300" type="image/jpeg">
<img src="logo.jpeg" class="center img-fluid" width="300" height="300">
</picture>


#### Wann und wo trefft ihr euch?

<!--
Das nächste Treffen findet am xx.xx.2022 ab 19:30 online unter https://meet.jit.si/chaostrefflb0001 statt.

Das nächste Treffen findet statt am Donnerstag, dem xx. MMM 2023 ab 18:30 Uhr im [Blauen Engel](https://www.blauerengel-ludwigsburg.de/) in Ludwigsburg. 
-->

Das nächste Treffen findet statt am Donnerstag, dem **25. Januar 2024 ab 18:30 Uhr** im [Blauen Engel](https://www.blauerengel-ludwigsburg.de/) in Ludwigsburg. 

Wir treffen uns in der Regel am letzten Donnerstag im Monat und ständig bei Matrix https://matrix.to/#/#chaostreff-lb:matrix.org 

Ihr erreicht uns außerdem per Mail über chaostreff AT complb PUNKt de.

Im Rahmen des Chaostreffs gibt es auch gelegentlich Kurzvorträge oder Ausflüge, mehr Infos dazu unter https://complb.de/vas.


#### Um was gehts?

Der Chaostreff Ludwigsburg ist ein lockeres und offenes Zusammentreffen von Menschen, die sich dem CCC und der [Hackerethik](https://www.ccc.de/de/hackerethik) nahe fühlen.

Neugierige und Interessierte, die sich für Themen rund um Technik und Datenschutz interessieren, sind uns stets willkommen!
