---
date: "2020-04-22"
title: "Veranstaltungen"
---

Der Chaostreff LB bietet immer wieder kürzere und längere Vorträge und andere Veranstaltungen an, vergangene und zukünftige Talks findet ihr hier.


1. 01.04.2020 - Harvey/Heinz: _Shell-Health-Check_ oder _Wie ich (wieder) lernte, die Shell zu lieben_ [Folien](https://complb.de/stammtisch20200401/CompLB-Kramski-Shell-Check-20200325_v02.pdf)
2. 22.04.2020 - ampoff/Steffen: _Die National Software Reference Library_ [Folien](https://complb.de/stammtisch20200422/nsrl_short.pdf)
3. 06.05.2020 - fritzthekit - _SIMD und neuronale Netze_
4. 19.05.2020 - Harvey/Heinz: _Mit Spielfilm-Mitschnitten gegen den Stream schwimmen_ [Folien](https://complb.de/stammtisch20200519/CompLB-Kramski-Home-Recording-20200519_v01.2.pdf)
5. 21.07.2020 - ampoff/Steffen: _Ansible und AWX_ oder _Langweilige Tasks automatisieren, mehr Zeit für alles andere_ [Folien](https://complb.de/stammtisch20200721/chaostreffLB_ansible_20200721.pdf)
6. 08.12.2020 - all: _GnuPG-Key-Signing-Party_ 
7. 02.10.2021 - all: Gemeinsamer Ausflug nach Tripsdrill mit Technikführung
8. 14.11.2021 - all: Gemeinsamer Ausflug zur Experimenta Heilbronn und zum dortigen Makerspace
9. 23.11.2021 - all: Gemeinsamer Ausflug mit dem [CCCS](https://www.cccs.de) zum Planetarium Stuttgart mit Technikführung
10. 12.02.2022 - all: Gemeinsamer Ausflug mit dem [CCCS](https://www.cccs.de) zum Technikmuseum Sinsheim mit IMAX 3D